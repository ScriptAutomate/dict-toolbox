---
minimum_pre_commit_version: 2.10.1
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.0.1
    hooks:
      - id: check-merge-conflict
        description: Check for files that contain merge conflict strings.
        language_version: python3
      - id: trailing-whitespace
        description: Trims trailing whitespace.
        args: [--markdown-linebreak-ext=md]
        language_version: python3
      - id: mixed-line-ending
        description: Replaces or checks mixed line ending.
        args: [--fix=lf]
        exclude: make.bat
        language_version: python3
      - id: fix-byte-order-marker
        description: Removes UTF-8 BOM if present, generally a Windows problem.
        language_version: python3
      - id: end-of-file-fixer
        description: Makes sure files end in a newline and only a newline.
        exclude: tests/fake_.*\.key
        language_version: python3
      - id: check-ast
        description: Simply check whether files parse as valid python.
        language_version: python3
      - id: check-yaml
      - id: check-json

  # ----- Formatting ---------------------------------------------------------------------------->
  - repo: https://github.com/myint/autoflake
    rev: v1.4
    hooks:
      - id: autoflake
        name: Remove unused variables and imports
        language: python
        args: ["--in-place", "--remove-all-unused-imports", "--remove-unused-variables", "--expand-star-imports"]
        files: \.py$

  - repo: https://github.com/saltstack/pre-commit-remove-import-headers
    rev: 1.1.0
    hooks:
      - id: remove-import-headers

  - repo: https://github.com/asottile/pyupgrade
    rev: v2.23.0
    hooks:
      - id: pyupgrade
        name: Rewrite Code to be Py3.6+
        args: [--py36-plus]

  - repo: https://github.com/asottile/reorder_python_imports
    rev: v2.6.0
    hooks:
      - id: reorder-python-imports
        args: [--py36-plus]

  - repo: https://github.com/psf/black
    rev: 21.7b0
    hooks:
      - id: black
        args: []

        name: Py3.7 Test Requirements

  - repo: https://github.com/PyCQA/bandit
    rev: "1.7.0"
    hooks:
      - id: bandit
        name: Run bandit against POP project
        args: [--silent, -lll]
        files: .*\.py
        exclude: >
            (?x)^(
                tests/.*
            )$
