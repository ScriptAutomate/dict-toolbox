# -*- coding: utf-8 -*-
import logging
from dict_tools import args

log = logging.getLogger(__name__)


def test_yamlify_arg():
    """
    Test that we properly yamlify CLI input. In several of the tests below
    assertIs is used instead of assertEqual. This is because we want to
    confirm that the return value is not a copy of the original, but the
    same instance as the original.
    """

    def _yamlify_arg(item):
        log.debug("Testing yamlify_arg with %r", item)
        return args.yamlify_arg(item)

    # Make sure non-strings are just returned back
    for item in (True, False, None, 123, 45.67, ["foo"], {"foo": "bar"}):
        assert _yamlify_arg(item) is item

    # Make sure whitespace-only isn't loaded as None
    for item in ("", "\t", " "):
        assert _yamlify_arg(item) is item

    # This value would be loaded as an int (123), the underscores would be
    # ignored. Test that we identify this case and return the original
    # value.
    item = "1_2_3"
    assert _yamlify_arg(item) is item

    # The '#' is treated as a comment when not part of a data structure, we
    # don't want that behavior
    for item in ("# hash at beginning", "Hello world! # hash elsewhere"):
        assert _yamlify_arg(item) is item

    # However we _do_ want the # to be intact if it _is_ within a data
    # structure.
    item = '["foo", "bar", "###"]'
    assert _yamlify_arg(item) == ["foo", "bar", "###"]
    item = '{"foo": "###"}'
    assert _yamlify_arg(item) == {"foo": "###"}

    # The string "None" should load _as_ None
    assert _yamlify_arg("None") is None

    # Leading dashes, or strings containing colons, will result in lists
    # and dicts, and we only want to load lists and dicts when the strings
    # look like data structures.
    for item in ("- foo", "foo: bar"):
        assert _yamlify_arg(item) is item

    # Make sure we don't load '|' as ''
    item = "|"
    assert _yamlify_arg(item) is item

    # Make sure we don't load '!' as something else (None in 2018.3, '' in newer)
    item = "!"
    assert _yamlify_arg(item) is item

    # Make sure we load ints, floats, and strings correctly
    assert _yamlify_arg("123") == 123
    assert _yamlify_arg("45.67") == 45.67
    assert _yamlify_arg("foo") == "foo"

    # We tested list/dict loading above, but there is separate logic when
    # the string contains a '#', so we need to test again here.
    assert _yamlify_arg('["foo", "bar"]') == ["foo", "bar"]
    assert _yamlify_arg('{"foo": "bar"}') == {"foo": "bar"}

    # Make sure that an empty string is loaded properly.
    assert _yamlify_arg("   ") == "   "

    # Make sure that we don't improperly load strings that would be
    # interpreted by PyYAML as YAML document start/end.
    assert _yamlify_arg("---") == "---"
    assert _yamlify_arg("--- ") == "--- "
    assert _yamlify_arg("...") == "..."
    assert _yamlify_arg(" ...") == " ..."

    # Make sure that non-printable whitespace is not YAML-loaded
    assert _yamlify_arg("foo\t\nbar") == "foo\t\nbar"
